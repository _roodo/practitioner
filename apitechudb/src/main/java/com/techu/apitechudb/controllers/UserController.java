package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.apache.catalina.User;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(){
        System.out.println("getUsers");

        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
    }

    /*@GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam( name = "$orderby", required = false) String orderBy) {
        System.out.println("getUsers");
        System.out.println("El valor del $orderby es " + orderBy);

        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }*/

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("El identificador del nuevo usuario es: "+user.getId());
        System.out.println("El nombre del nuevo usuario es: "+user.getName());
        System.out.println("La edad del nuevo usuario es "+user.getAge());

        return new ResponseEntity<>(this.userService.addUser(user), HttpStatus.CREATED);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserByID(@PathVariable String id){
        System.out.println("getUserByID");
        System.out.println("El identificador del usuario que se desea buscar es: " +id);

        Optional<UserModel> result = this.userService.findByID(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario No Encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@PathVariable String id, @RequestBody UserModel user){
        System.out.println("updateUser");
        System.out.println("El identificador del usuario que se va a actualizar en parámetro URL es "+id);
        System.out.println("El identificador del usuario que se va a actualizar es "+user.getId());
        System.out.println("El nombre del usuario que se va a actualizar es "+user.getName());
        System.out.println("La edad del usuario que se va a actualizar es "+user.getAge());

        Optional<UserModel> userToUpdateAll = this.userService.findByID(id);
        if(userToUpdateAll.isPresent()){
            System.out.println("Usuario par actualizar encontrado, actualizando...");
            this.userService.update(user);
        }

        return new ResponseEntity<>(user, userToUpdateAll.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUserP(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUserP");
        System.out.println("El usuario a actualizar en parámetro de URL es "+id);
        System.out.println("El nombre nuevo del usuario es "+user.getName());
        System.out.println("La edad nueva del usuario es "+user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findByID(id);
        if(userToUpdate.isPresent() == true){
            System.out.println("Usuario para actualizar encontrado, actualizando...");
            this.userService.updateUserP(user);
        }

        return new ResponseEntity<>(user, userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>(
                    deletedUser ? "Usuario Eliminado" : "Usuario No Eliminado",
                    deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
