package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductByID(@PathVariable String id){
        System.out.println("getProductByID");
        System.out.println("El identificador del producto que se quiere buscar es "+id);

        Optional<ProductModel> result = this.productService.findByID(id);

        return new ResponseEntity(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es: "+product.getId());
        System.out.println("La descripción del producto que se va a crear es: "+product.getDesc());
        System.out.println("El precio del producto que se va a crear es: "+product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("El identificador del producto a actualizar en parámetro de URL es "+id);
        System.out.println("El identificador nuevo del producto es "+product.getId());
        System.out.println("La descripción nueva del producto es "+product.getDesc());
        System.out.println("El precio nuevo del producto es "+product.getPrice());


        Optional<ProductModel> productToUpdate = this.productService.findByID(id);
        if(productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando...");
            this.productService.update(product);
        }

        return new ResponseEntity<>(product, productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");

        boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deletedProduct ? "Producto Borrado" : "Producto No Borrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}