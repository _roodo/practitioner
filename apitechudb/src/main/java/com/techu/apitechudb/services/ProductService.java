package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll ProductService");

        return this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product){
        System.out.println("add en ProductService");

        return this.productRepository.save(product);
    }

    /*Optional está pensando para ser un tipo de retorno de función*/
    public Optional<ProductModel> findByID(String id){
        System.out.println("findByID en ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel product){
        System.out.println("update en ProductService");

        return this.productRepository.save(product);
    }

    public boolean delete(String id){
        System.out.println("delete en ProductService");
        boolean result = false;

        if(this.findByID(id).isPresent() == true){
            System.out.println("Producto encontrado, borrando...");
            this.productRepository.deleteById(id);
            result = true;
        }
        /*No se usa porque no sabes qué más cosas hace*/
        /*this.productRepository.findById();*/

        return result;
    }
}
