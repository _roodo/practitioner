package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll UserService");

        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
    }


    /*public List<UserModel> findAll(String orderBy) {
        System.out.println("findAll UserService");

        List<UserModel> result;
        if(orderBy != null){
            System.out.println("Se ha pedido ordenación");
            result = this.userRepository.findAll(Sort.by("age"));
        }else{
            result = this.userRepository.findAll();
        }

        return result;
    }*/

    public UserModel addUser(UserModel user){
        System.out.println("addUser en UserService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findByID(String id){
        System.out.println("findByID en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel user){
        System.out.println("update en UserService");

        return this.userRepository.save(user);
    }

    public UserModel updateUserP(UserModel user){
        System.out.println("updateUserP en UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");
        boolean result = false;

        if(this.findByID(id).isPresent() == true){
            System.out.println("Usuario encontrado, borrando...");
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
